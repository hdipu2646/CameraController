[v1.3.2](#v1.3.2) - 18th September 2022
### Added
- Upgraqde to turn off camera control logic while hovering on a UI

### Changes
- 

[v1.3.1](#v1.3.1) - 18th September 2022
### Added
- OrbitalPositioning Coroutine Added
- Rotation Functionality Improved.

### Changes
- WebGL and Unity Editor Logic Change with Target Distance for Rotation Functionality Improvement.

[v1.3.0](#v1.3.0) - 17th September 2022
### Added

### Changes
- Package Name changed to Camera Controller
- Git Repository Name Changed to Camera Controller
- Git URL Changed to https://gitlab.com/UnityPackage/CameraController.git

[v1.2.0](#v1.2.0) - 11st September 2022
### Added
- Documentation Added

### Changes
- No Changes

[v1.1.0](#v1.1.0) - 04th September 2022
### Added
- Handle Target Object is Empty or Not
- Code Optimized with Ternary Operator

### Changes
- No Changes

[v1.0.5](#v1.0.5) - 01st September 2022
### Added
- Remove Rotation Smoothnees for WebGL.

### Changes
- Removed - Approximately calculation.

[v1.0.4](#v1.0.4) - 01st September 2022
### Added
- Rotation Method Updated.

### Changes
- Approximately calculation is now inside own technique

[v1.0.3](#v1.0.3) - 01st September 2022
### Added
- Camera Zooming smoothness Upgraded.
- Panning Smoothness Upgraded.
- Panning Permormance upgraded.

### Changes
- Cursor showhide Method upgraded
- Some variable name Changed

[v1.0.2](#v1.0.2) - 07th August 2022
### Added
- Camera Rotation Smoothness Upgraded

### Changes
- Distance Calcultation start on now Start Function
- Distance Calcultation Continuously when Zooming only

[v1.0.1](#v1.0.1) - 07th August 2022
### Added
- x Delta and y Delta exposed to panning zoomming and Rotating Function

### Changes
- Feature initialized

[v1.0.0](#v1.0.0) - 07th August 2022
### Added
- Feature initialized
**- While Camera Rotation Start Reset the Camera position to Camera Look At to the Object.**

### Changes
- Feature initialized