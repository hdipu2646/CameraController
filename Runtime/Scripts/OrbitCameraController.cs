namespace Walid.Camera
{
    using System;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class OrbitCameraController : MonoBehaviour
    {
        public enum MouseButton
        {
            MouseLeft,
            MouseRight,
            MouseMiddle,
        }
        public Transform target;
        public Camera orbitCamera;
        public bool isCursorHide = true;
        public MouseButton panClick, rotateClick;
        public float targetDistance, smoothnessSensivity = 5f, panSensitivity = 80f, zoomSensitivity = 200f, rotateSensitivity = 80f, xAxisMinRotation = -90f, xAxisRotationMax = 90f;
        private Quaternion rotation = Quaternion.identity;
        private Vector3 smoothVelocity = Vector3.zero; //Will be Used If we want to use SmoothDamping
        private Vector3 additionalDirectionNDistance, targetPosition;
        private bool isNotInOrbit;

        #region Public Methods
        /// <summary>
        /// Zooming Camera to Forward Direction
        /// </summary>
        /// <param name="zoomRate">Negetive value will Zoom Out Positive will Zoom In (Value Clamped in -0.3 to 0.3)</param>
        public void Zoomming(float zoomRate)
        {
            zoomRate = Mathf.Clamp(zoomRate, -0.3f, 0.3f);
            CalculateTargetDistance(zoomRate);
            targetPosition = Vector3.MoveTowards(orbitCamera.transform.position, target ? target.position : Vector3.zero, zoomRate * zoomSensitivity);
            orbitCamera.transform.position = Vector3.Lerp(orbitCamera.transform.position, targetPosition, Time.deltaTime * smoothnessSensivity);
        }
        /// <summary>
        /// Panning Camera on Left Right Up & Down
        /// </summary>
        /// <param name="xDelta">Negetive value will move to Right Positive will move to Left</param>
        /// <param name="yDelta">Negetive value will move to Up Positive will move to Down</param>
        public void Panning(float xDelta, float yDelta)
        {
            targetPosition = new Vector3(-xDelta * Time.deltaTime * panSensitivity, -yDelta * Time.deltaTime * panSensitivity, 0);
            additionalDirectionNDistance = Vector3.Lerp(additionalDirectionNDistance, targetPosition, Time.deltaTime * smoothnessSensivity);
            orbitCamera.transform.Translate(additionalDirectionNDistance);
            isNotInOrbit = true;
        }
        /// <summary>
        /// Rotating Camera according to Target Object to Left Right Up & Down
        /// </summary>
        /// <param name="xDelta">Negetive value will move to Right Positive will move to Left</param>
        /// <param name="yDelta">Negetive value will move to Up Positive will move to Down</param>
        public void Rotating(float xDelta, float yDelta)
        {
            rotation.x -= yDelta * Time.deltaTime * rotateSensitivity;
            rotation.y += xDelta * Time.deltaTime * rotateSensitivity;
            rotation.x = Mathf.Clamp(rotation.x, xAxisMinRotation, xAxisRotationMax);
            Quaternion targetRotation = Quaternion.Euler(rotation.x, rotation.y, 0f);
            Vector3 negDistance = new Vector3(0f, 0f, -targetDistance);
            targetPosition = targetRotation * negDistance + (target ? target.position : Vector3.zero);
            float targetDistanceNowFromCamera = Vector3.Distance(transform.position, target.position);
            if (isNotInOrbit && Mathf.Abs(targetDistanceNowFromCamera - targetDistance) > 0.01f)
            {
                orbitCamera.transform.rotation = Quaternion.Slerp(orbitCamera.transform.rotation, targetRotation, Time.deltaTime * smoothnessSensivity);
                orbitCamera.transform.position = Vector3.Slerp(orbitCamera.transform.position, targetPosition, Time.deltaTime * smoothnessSensivity);
            }
            else
            {
                isNotInOrbit = false;
                orbitCamera.transform.rotation = targetRotation;
                orbitCamera.transform.position = targetPosition;
            }
        }

        public void SetCameraPosition(Action callBack)
        {
            StartCoroutine(OrbitalPositioning(callBack: callBack));
        }
        #endregion Public Methods

        #region Private Methods
        #region Unity Defaults Methods
        private void Start()
        {
            CalculateTargetDistance();
        }
        private void Update()
        {
            ShowHideCursor();
        }
        private void LateUpdate()
        {
            if (EventSystem.current.IsPointerOverGameObject()) return;
            Zoomming(Input.GetAxis(axisName: "Mouse ScrollWheel"));
            if (Input.GetMouseButton((int)panClick))
                Panning(xDelta: Input.GetAxis(axisName: "Mouse X"), yDelta: Input.GetAxis(axisName: "Mouse Y"));
            if (Input.GetMouseButton((int)rotateClick))
                Rotating(xDelta: Input.GetAxis(axisName: "Mouse X"), yDelta: Input.GetAxis(axisName: "Mouse Y"));
        }
        #endregion Unity Defaults Methods
        /// <summary>
        /// Calculating Target Distance from Camera
        /// </summary>
        /// <param name="zoomRate">Provide Zooming Rate</param>
        private void CalculateTargetDistance(float zoomRate = 0.2f)
        {
            targetDistance = Mathf.Abs(zoomRate) > 0 ? Vector3.Distance(orbitCamera.transform.position, (target ? target.position : Vector3.zero)) : targetDistance;
        }
        private void ShowHideCursor()
        {
            if (isCursorHide && (Input.GetMouseButtonDown((int)panClick) || Input.GetMouseButtonDown((int)rotateClick)))
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            if (isCursorHide && (Input.GetMouseButtonUp((int)panClick) || Input.GetMouseButtonUp((int)rotateClick)))
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        private IEnumerator OrbitalPositioning(Action callBack)
        {
            float targetDistanceNowFromCamera = 0;
            while (Mathf.Abs(targetDistanceNowFromCamera - targetDistance) > 0.01f)
            {
                targetDistanceNowFromCamera = Vector3.Distance(orbitCamera.transform.position, target.position);
                Quaternion targetRotation = orbitCamera.transform.rotation;
                Vector3 negDistance = new Vector3(0f, 0f, -targetDistance);
                targetPosition = targetRotation * negDistance + (target ? target.position : Vector3.zero);
                orbitCamera.transform.rotation = Quaternion.Slerp(orbitCamera.transform.rotation, targetRotation, Time.deltaTime * smoothnessSensivity);
                orbitCamera.transform.position = Vector3.Slerp(orbitCamera.transform.position, targetPosition, Time.deltaTime * smoothnessSensivity);
                yield return new WaitForEndOfFrame();
            }
            callBack?.Invoke();
        }
        #endregion Private Methods
    }
}